import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.mongodb.MongoException
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters.*
import org.bson.Document

// Variables globales para utilizar en el ejercicio 2, para formatear el json
val gson: Gson = GsonBuilder().setPrettyPrinting().create()
val jsonParser = JsonParser()

fun main() {
    // Conexion al cluster "SharedCluster" de MongoDB.
    val uri = "mongodb+srv://joaoRomanAlejandro:ITB_2023@sharedcluster.limu8an.mongodb.net/?retryWrites=true&w=majority"
    val mongodbClient = MongoClients.create(uri)
    // Obtención de la base de datos "sample_training", que está dentro del cluster
    val db = mongodbClient.getDatabase("sample_training")
    // Obtención de la colección "grades" de la base de datos
    val collection = db.getCollection("grades")

    exercici1(collection)
    exercici2point1(collection)
    exercici2point2(collection)
    exercici2point3(collection)
    exercici2point4(collection)
}

private fun exercici1(collection: MongoCollection<Document>){
    // Declaración de una lista mutable para añadir posteriormente los objetos json
    val listOfDocuments = mutableListOf<Document>()

    // Inserción de los objetos json a la base de datos y control de excepciones
    try {
        val string1 = """{"student_id": 111333444, "name": "Alejandro", "surname": "Arcas Leon", "class_id": "DAM", "group": "DAMi", "scores": [{"type": "exam", "score": 100},{"type": "teamWork","score": 50}]}"""
        val string2 = """{"student_id": 111222333,"name": "Joao","surname": "Lopes","class_id": "DAM","group": "DAMr","interests": ["music", "gym", "code", "electronics"]}"""
        val firstDocument = Document.parse(string1)
        val secondDocument = Document.parse(string2)

        listOfDocuments.add(firstDocument)
        listOfDocuments.add(secondDocument)

        collection.insertMany(listOfDocuments)

    } catch (e: MongoException) {
    println(e.message)
    }
}

private fun exercici2point1(collection: MongoCollection<Document>){
    // Creación del filtro para buscar registros
    val queryFilter = eq("group", "DAMi")
    val query = collection.find(queryFilter).iterator() // Buscar registro con el filtro declarado anteriormente
    // Iteración de los resultados e imprimir por pantalla el json formateado
    while (query.hasNext()){
        val json = query.next().toJson()
        jsonStringToPrint(json)
    }
}

private fun exercici2point2(collection: MongoCollection<Document>){
    // Creación del filtro para buscar registros
    val queryFilter = elemMatch("scores", and(eq("type", "exam"), eq("score", 100)))
    val query = collection.find(queryFilter).iterator() // Buscar registro con el filtro declarado anteriormente
    // Iteración de los resultados e imprimir por pantalla el json formateado
    while (query.hasNext()){
        val json = query.next().toJson()
        jsonStringToPrint(json)
    }
}

private fun exercici2point3(collection: MongoCollection<Document>){
    // Creación del filtro para buscar registros
    val queryFilter = elemMatch("scores", and(eq("type", "exam"), lt("score", 50)))
    val query = collection.find(queryFilter).iterator() // Buscar registro con el filtro declarado anteriormente
    // Iteración de los resultados e imprimir por pantalla el json formateado
    while (query.hasNext()){
        val json = query.next().toJson()
        jsonStringToPrint(json)
    }
}

private fun exercici2point4(collection: MongoCollection<Document>){
    // Creación del filtro para buscar registros
    val queryFilter = eq("student_id", 111222333)
    // Buscar registro con el filtro declarado anteriormente y además indicar una proyección para mostrar solo los intereses
    val query = collection.find(queryFilter).projection(Document.parse("""{interests: 1, _id: 0}""")).iterator()
    // Iteración de los resultados e imprimir por pantalla el json formateado
    while (query.hasNext()){
        val json = query.next().toJson()
        jsonStringToPrint(json)
    }
}

// Funcion para imprimir el json para que sea legible
private fun jsonStringToPrint(json: String){
    val jsonElement = jsonParser.parse(json)
    val prettyJsonString = gson.toJson(jsonElement)
    println(prettyJsonString)
}
